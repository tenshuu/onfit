<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Facade\FlareClient\Api;

class UserController extends ApiController
{
    public $loginAfterSignUp = true;

    public function register(Request $request) {
        $password = $request->get('password');
        $email = $request->get('email');

        if ($role = $request->get('role') == 'coach') {
            $role = 2;
        }
        else {$role = 1;}


        if ($this->checkIfUserExist($email)){
            return response()->json([
                'message' => 'User already exist'
            ],500);
        } else {
            $user = User::create([
                   'name' => $request->input('name'),
                    'password' => bcrypt($password),
                    'email' => $email,
                    'role' => $role,
            ]);
            $token = auth()->login($user);
            return $this->respondWithToken($token);
        }
    }

    private function checkIfUserExist($email) {
        $user = User::where('email', $email)->first();

        if ($user) {
            return $user;
        } else {
            return false;
        }
    }

    public function login(Request $request) {
        $credentials = $request->only(['email', 'password']);

        if(!$token = auth()->attempt($credentials)){
            return response()->json(['error'=> 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    public function updateToken(Request $request) {
        $id = $request->get('id');
        $token = $request->get('token');

        User::where('id', $id)->update([
            'authToken' => $token
        ]);
    }

    public function getAuthUser(Request $request) {
        return response()->json(auth()->user());
    }

    public function logout() {
        auth()->logout();
        return response()->json(['message'=>'Successfully logged out']);

    }

    protected function respondWithToken($token){
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => '1h'
            //'expires_in' => auth('api')->factory()->getTTL()*60
        ]);
    }

    public function getAllUsers() {
        $users = User::all();

        return $this->sendResponse($users, 'OK', 200);
    }


}
